package com.google.gwt.sample.guitarwarehouse.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

import com.google.gwt.sample.guitarwarehouse.client.GreetingService;
import com.google.gwt.sample.guitarwarehouse.client.Guitar;
import com.google.gwt.sample.guitarwarehouse.client.User;
import com.google.gwt.sample.guitarwarehouse.client.UserRole;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {

	private ArrayList<User> userInfoList = new ArrayList<User>();

	@Override
	public Guitar addGuitarInfoToFile(Guitar guitarInfo) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter("D:\\User\\eclipse-workspace\\guitarInfo.txt", true));

		bw.append("{ ");
		bw.append("name: " + guitarInfo.getName() + " ");
		bw.append("color: " + guitarInfo.getColor() + " ");
		bw.append("number of strings: " + guitarInfo.getNumberOfGuitarStrings() + " ");
		bw.append("quantity: " + guitarInfo.getQuantity() + " ");
		bw.append("type: " + guitarInfo.getType() + " ");
		bw.append("price: " + guitarInfo.getPrice() + " ");
		bw.append("}");
		bw.newLine();
		bw.close();
		return guitarInfo;
	}

	@Override
	public String updateGuitarInfoInsideTheFile(Guitar oldGuitarInfo, Guitar newGuitarInfo) {
		File guitarInfo = new File("D:\\User\\eclipse-workspace\\guitarInfo.txt");
		try {
			File temp = File.createTempFile("guitarInfo", ".txt", guitarInfo.getParentFile());
			String charset = "UTF-8";
			String oldInfo = "{ name: " + oldGuitarInfo.getName() + " color: " + oldGuitarInfo.getColor()
					+ " number of strings: " + oldGuitarInfo.getNumberOfGuitarStrings() + " quantity: "
					+ oldGuitarInfo.getQuantity() + " type: " + oldGuitarInfo.getType() + " price: "
					+ oldGuitarInfo.getPrice() + " }";
			String newInfo = "{ name: " + newGuitarInfo.getName() + " color: " + newGuitarInfo.getColor()
					+ " number of strings: " + newGuitarInfo.getNumberOfGuitarStrings() + " quantity: "
					+ newGuitarInfo.getQuantity() + " type: " + newGuitarInfo.getType() + " price: "
					+ newGuitarInfo.getPrice() + " }";
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(guitarInfo), charset));
			PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(temp), charset));
			for (String line; (line = reader.readLine()) != null;) {
				line = line.replace(oldInfo, newInfo);
				writer.println(line);
			}
			reader.close();
			writer.close();
			guitarInfo.delete();
			temp.renameTo(guitarInfo);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String deleteGuitarInfoFromFile(String guitarName, String guitarColor, String numberOfGuitarStrings,
			String guitarQuantity, String guitarType, String guitarPrice) {
		File guitarInfo = new File("D:\\User\\eclipse-workspace\\guitarInfo.txt");
		try {
			File temp = File.createTempFile("guitarInfo", ".txt", guitarInfo.getParentFile());
			String charset = "UTF-8";
			String delete = "{ name: " + guitarName + " color: " + guitarColor + " number of strings: "
					+ numberOfGuitarStrings + " quantity: " + guitarQuantity + " type: " + guitarType + " price: "
					+ guitarPrice + " }";
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(guitarInfo), charset));
			PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(temp), charset));
			for (String line; (line = reader.readLine()) != null;) {
				line = line.replace(delete, "");
				writer.println(line);
			}
			reader.close();
			writer.close();
			guitarInfo.delete();
			temp.renameTo(guitarInfo);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return "Hello";
	}

	@Override
	public String addUserInfoToList(String userName, String userPassword) {
		User userInfo = new User();
		userInfo.setName(userName);
		userInfo.setPassword(userPassword);
		userInfo.setRole(UserRole.USER);
		userInfoList.add(userInfo);
		return "Success";
	}

	@Override
	public String chekIfUserExist(String userName, String userPassword) {
		for (User userInfo : userInfoList) {
			if (userInfo.getName().equals(userName) && userInfo.getPassword().equals(userPassword)) {
				return "Welcome";
			}
		}
		return "Invalid credentials";
	}

	@Override
	public String addPurchasesToUser(String userName, Guitar guitar) {
		for (User user : userInfoList) {
			if (user.getName().equals(userName)) {
				user.setPurchases(guitar);
				break;
			}
		}
		return "Success";
	}

	@Override
	public ArrayList<Guitar> getPurchasesOfUser(String userName) {
		for (User user : userInfoList) {
			if (user.getName().equals(userName)) {
				return user.getPurchases();
			}
		}
		return null;
	}

}
