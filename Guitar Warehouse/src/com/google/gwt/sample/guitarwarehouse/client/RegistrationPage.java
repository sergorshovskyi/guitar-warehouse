package com.google.gwt.sample.guitarwarehouse.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class RegistrationPage {
	public RegistrationPage() {
		buildContent();
	}

	private VerticalPanel registrationPanel = new VerticalPanel();
	private Label registrationLabel = new Label("Registration page");

	public static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	private void buildContent() {

		HorizontalPanel hPanelForButtons = new HorizontalPanel();
		Button signInButton = new Button("Sign in");
		Button signUpButton = new Button("Sign up");
		TextBox nameField = new TextBox();
		PasswordTextBox passwordField = new PasswordTextBox();

		hPanelForButtons.add(signUpButton);
		hPanelForButtons.add(signInButton);

		registrationLabel.setStyleName("loginHeading");
		registrationPanel.add(new Label("Enter your name:"));
		registrationPanel.add(nameField);
		registrationPanel.add(new Label("Create a password:"));
		registrationPanel.add(passwordField);
		registrationPanel.setStyleName("loginPanel");
		registrationPanel.add(hPanelForButtons);
		registrationPanel.setSpacing(6);

		signInButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				GuitarWarehouse.hideRegistrationPage();
				GuitarWarehouse.showLoginPage();
			}
		});

		ArrayList<String> namesList = new ArrayList<String>();

		signUpButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (!nameField.getText().matches("^[0-9a-zA-Z\\.]{4,30}$")) {
					Window.alert("'" + nameField.getText()
							+ "' is not a valid name. Name should contain only letters and numbers. Minimum length is 4 characters");
					nameField.selectAll();
					return;
				}
				if (!passwordField.getText().matches("^[0-9a-zA-Z\\.]{6,30}$")) {
					Window.alert("'" + passwordField.getText()
							+ "' is not a valid password. Password should contain only letters and numbers. Minimum length is 6 characters");
					passwordField.selectAll();
					return;
				}
				User userInfo = new User();
				if (!namesList.contains(nameField.getText())) {
					namesList.add(nameField.getText());
					userInfo.setName(nameField.getText());
					userInfo.setPassword(passwordField.getText());
					userInfo.setRole(UserRole.USER);
					createUser(userInfo);
					UsersListPage.addUserToTable(userInfo.getName());
					BasketPage.setCurentUserName(userInfo.getName());
					nameField.setText("");
					passwordField.setText("");
				} else {
					Window.alert("User '" + nameField.getText() + "' already exist");
				}

			}
		});
	}

	public void createUser(User userInfo) {

		greetingService.addUserInfoToList(userInfo.getName(), userInfo.getPassword(), new AsyncCallback<String>() {

			@Override
			public void onSuccess(String result) {
				Window.alert("User '" + userInfo.getName() + "' was successfully created");
				GuitarWarehouse.hideRegistrationPage();
				GuitarWarehouse.showUserMainPage();

			}

			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.toString());
			}
		});
	}

	public void show() {
		RootPanel.get().add(registrationPanel);
		RootPanel.get().add(registrationLabel);
	}

	public void hide() {
		RootPanel.get().remove(registrationPanel);
		RootPanel.get().remove(registrationLabel);
	}
}
