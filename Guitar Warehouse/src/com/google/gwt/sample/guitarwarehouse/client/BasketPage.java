package com.google.gwt.sample.guitarwarehouse.client;

import java.util.ArrayList;
import java.util.Collections;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.TableRowElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class BasketPage {
	public BasketPage() {
		buildContent();
	}

	private static FlexTable flexTable = new FlexTable();
	private static VerticalPanel basketPanel = new VerticalPanel();
	private static String curentUserName;

	private static TableRowElement findNearestParentRow(Node node) {
		Node element = findNearestParentNodeByType(node, "tr");
		if (element != null) {
			return element.cast();
		}
		return null;
	}

	private static Node findNearestParentNodeByType(Node node, String nodeType) {
		while ((node != null)) {
			if (Element.is(node)) {
				Element elem = Element.as(node);

				String tagName = elem.getTagName();

				if (nodeType.equalsIgnoreCase(tagName)) {
					return elem.cast();
				}

			}
			node = node.getParentNode();
		}
		return null;
	}

	private void buildContent() {

		Button goBackButton = new Button("Go back");
		Button confirmOrderButton = new Button("Confirm order");

		flexTable.setText(0, 0, "Title");
		flexTable.setText(0, 1, "Color");
		flexTable.setText(0, 2, "Quantity");
		flexTable.setText(0, 3, "Type");
		flexTable.setText(0, 4, "Srings");
		flexTable.setText(0, 5, "Price");
		flexTable.setCellPadding(5);
		flexTable.setBorderWidth(1);
		flexTable.getRowFormatter().addStyleName(0, "watchListHeader");

		basketPanel.add(goBackButton);
		basketPanel.add(flexTable);
		basketPanel.add(confirmOrderButton);

		ArrayList<Integer> indexes = new ArrayList<>();

		goBackButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (int i = 1; i < UserMainPage.getRowCount(); i++) {
					if (UserMainPage.getTableText(i, 2).equals("0")) {
						indexes.add(i);
					}
				}
				if (!indexes.isEmpty()) {
					Collections.sort(indexes);
					for (int j = indexes.size() - 1; j >= 0; j--) {
						UserMainPage.removeTableRow(indexes.get(j));
						AdminMainPage.removeTableRow(indexes.get(j));
					}
					indexes.clear();
				}

				hide();
				GuitarWarehouse.showUserMainPage();
			}
		});

		confirmOrderButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				int row = flexTable.getRowCount();
				for (int i = 1; i < row; i++) {
					Guitar oldInfo = new Guitar();
					Guitar newInfo = new Guitar();
					for (int j = 1; j < UserMainPage.getRowCount(); j++) {
						if (flexTable.getText(i, 0).equals(UserMainPage.getTableText(j, 0))
								&& flexTable.getText(i, 1).equals(UserMainPage.getTableText(j, 1))
								&& flexTable.getText(i, 3).equals(UserMainPage.getTableText(j, 3))
								&& flexTable.getText(i, 4).equals(UserMainPage.getTableText(j, 4))) {
							Integer newQuantity = Integer.parseInt(UserMainPage.getTableText(j, 2))
									- Integer.parseInt(flexTable.getText(i, 2));

							oldInfo.setName(flexTable.getText(i, 0));
							oldInfo.setColor(flexTable.getText(i, 1));
							oldInfo.setType(flexTable.getText(i, 3));
							oldInfo.setNumberOfGuitarStrings(flexTable.getText(i, 4));
							Integer price = Integer.parseInt(flexTable.getText(i, 5))
									/ Integer.parseInt(flexTable.getText(i, 2));
							oldInfo.setPrice(price.toString());
							oldInfo.setQuantity(flexTable.getText(j, 2));

							if (newQuantity == 0) {
								RegistrationPage.greetingService.deleteGuitarInfoFromFile(flexTable.getText(i, 0),
										flexTable.getText(i, 1), flexTable.getText(i, 4),
										UserMainPage.getTableText(j, 2), flexTable.getText(i, 3), price.toString(),
										new AsyncCallback<String>() {

											@Override
											public void onSuccess(String result) {
												Window.alert("Guitar info successfully updated on the server!");
											}

											@Override
											public void onFailure(Throwable caught) {
												Window.alert(caught.toString());
											}
										});
								UserMainPage.setTableText(j, 2, newQuantity.toString());
								AdminMainPage.setTableText(j, 2, newQuantity.toString());
								break;
							}

							newInfo.setName(flexTable.getText(i, 0));
							newInfo.setColor(flexTable.getText(i, 1));
							newInfo.setType(flexTable.getText(i, 3));
							newInfo.setNumberOfGuitarStrings(flexTable.getText(i, 4));
							newInfo.setPrice(price.toString());
							newInfo.setQuantity(newQuantity.toString());

							RegistrationPage.greetingService.updateGuitarInfoInsideTheFile(oldInfo, newInfo,
									new AsyncCallback<String>() {

										@Override
										public void onSuccess(String result) {
											// Window.alert("Guitar info successfully updated on the server!");
										}

										@Override
										public void onFailure(Throwable caught) {
											Window.alert(caught.toString());
										}
									});

							oldInfo.setPrice(flexTable.getText(i, 5));
							RegistrationPage.greetingService.addPurchasesToUser(curentUserName, oldInfo,
									new AsyncCallback<String>() {

										@Override
										public void onFailure(Throwable caught) {
											Window.alert(caught.toString());
										}

										@Override
										public void onSuccess(String result) {
											if (result.equals("Success")) {
												Window.alert("Purchases added!");
											}

										}
									});
							UserMainPage.setTableText(j, 2, newQuantity.toString());
							AdminMainPage.setTableText(j, 2, newQuantity.toString());
							break;
						}
					}

				}
				while (flexTable.getRowCount() > 1) {
					flexTable.removeRow(1);
				}
			}

		});

	}

	public static int getRowCount() {
		return flexTable.getRowCount();
	}

	public static String getTableText(int row, int column) {
		return flexTable.getText(row, column);
	}

	public static void setTableText(int row, int column, String text) {
		flexTable.setText(row, column, text);
		Button deleteButton = new Button("Delete");
		flexTable.setWidget(row, 6, deleteButton);

		deleteButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				TableRowElement tableRow = findNearestParentRow(deleteButton.getElement());
				flexTable.removeRow(tableRow.getRowIndex());
			}
		});

	}

	public static void setCurentUserName(String name) {
		curentUserName = name;
	}

	public void show() {
		RootPanel.get().add(basketPanel);
	}

	public void hide() {
		RootPanel.get().remove(basketPanel);
	}
}
