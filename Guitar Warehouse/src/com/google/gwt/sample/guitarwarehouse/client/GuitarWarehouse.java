package com.google.gwt.sample.guitarwarehouse.client;

import com.google.gwt.core.client.EntryPoint;

public class GuitarWarehouse implements EntryPoint {

	private static LoginPage loginPage = new LoginPage();
	private static AdminMainPage adminMainPage = new AdminMainPage();
	private static RegistrationPage registrationPage = new RegistrationPage();
	private static UserMainPage userMainPage = new UserMainPage();
	private static BasketPage basketPage = new BasketPage();
	private static UsersListPage usersListPage = new UsersListPage();

	public void onModuleLoad() {
		loginPage.show();
	}

	public static void showLoginPage() {
		loginPage.show();
	}

	public static void showAdminMainPage() {
		adminMainPage.show();
	}

	public static void hideLoginPage() {
		loginPage.hide();
	}

	public static void hideAdminMainPage() {
		adminMainPage.hide();
	}

	public static void hideRegistrationPage() {
		registrationPage.hide();
	}

	public static void showRegistrationPage() {
		registrationPage.show();
	}

	public static void hideUserMainPage() {
		userMainPage.hide();
	}

	public static void showUserMainPage() {
		userMainPage.show();
	}

	public static void hideBasketPage() {
		basketPage.hide();
	}

	public static void showBasketPage() {
		basketPage.show();
	}

	public static void hideUsersListPage() {
		usersListPage.hide();
	}

	public static void showUsersListPage() {
		usersListPage.show();
	}
}
