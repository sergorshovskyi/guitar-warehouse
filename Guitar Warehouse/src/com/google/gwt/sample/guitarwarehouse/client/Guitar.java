package com.google.gwt.sample.guitarwarehouse.client;

import java.io.Serializable;

public class Guitar implements Serializable {
	private String name;
	private String quantity;
	private String type;
	private String numberOfGuitarStrings;
	private String color;
	private String price;

	public Guitar() {
	}

	public Guitar(String guitarName, String quantity, String type, String numberOfGuitarStrings, String color,
			String price) {
		this.name = guitarName;
		this.quantity = quantity;
		this.type = type;
		this.numberOfGuitarStrings = numberOfGuitarStrings;
		this.color = color;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String guitarName) {
		this.name = guitarName;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNumberOfGuitarStrings() {
		return numberOfGuitarStrings;
	}

	public void setNumberOfGuitarStrings(String numberOfGuitarStrings) {
		this.numberOfGuitarStrings = numberOfGuitarStrings;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

}
