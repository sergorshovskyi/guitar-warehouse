package com.google.gwt.sample.guitarwarehouse.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {

	void addGuitarInfoToFile(Guitar guitarInfo, AsyncCallback<Guitar> callback);

	void deleteGuitarInfoFromFile(String guitarName, String guitarColor, String numberOfGuitarStrings,
			String guitarQuantity, String guitarType, String guitarPrice, AsyncCallback<String> callback);

	void updateGuitarInfoInsideTheFile(Guitar oldGuitarInfo, Guitar newGuitarInfo, AsyncCallback<String> callback);

	void addUserInfoToList(String userName, String userPassword, AsyncCallback<String> callback);

	void chekIfUserExist(String userName, String userPassword, AsyncCallback<String> callback);

	void addPurchasesToUser(String userName, Guitar guitar, AsyncCallback<String> callback);

	void getPurchasesOfUser(String userName, AsyncCallback<ArrayList<Guitar>> callback);
}
