package com.google.gwt.sample.guitarwarehouse.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class LoginPage {
	public LoginPage() {
		buildContent();
	}

	private VerticalPanel loginPanel = new VerticalPanel();
	private Label loginLabel = new Label("Login page");
	private static boolean isAGuest = false;

	private void buildContent() {

		User admin = new User("admin", "password", UserRole.ADMIN);

		HorizontalPanel hPanelForButtons = new HorizontalPanel();
		Button signInButton = new Button("Sign in");
		Button signUpButton = new Button("Sign up");
		Button guestButton = new Button("I'm a guest");
		TextBox nameField = new TextBox();
		PasswordTextBox passwordField = new PasswordTextBox();

		hPanelForButtons.add(signInButton);
		hPanelForButtons.add(signUpButton);
		hPanelForButtons.add(guestButton);

		loginLabel.setStyleName("loginHeading");
		loginPanel.add(new Label("Name:"));
		loginPanel.add(nameField);
		loginPanel.add(new Label("Password:"));
		loginPanel.add(passwordField);
		loginPanel.setStyleName("loginPanel");
		loginPanel.add(hPanelForButtons);
		loginPanel.setSpacing(6);

		signInButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (nameField.getText().equals(admin.getName())
						&& passwordField.getText().equals(admin.getPassword())) {

					hide();
					GuitarWarehouse.showAdminMainPage();
					nameField.setText("");
					passwordField.setText("");

					return;
				}
				checkIfUserExist(nameField.getText(), passwordField.getText());
				BasketPage.setCurentUserName(nameField.getText());
				nameField.setText("");
				passwordField.setText("");
			}
		});

		signUpButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				GuitarWarehouse.hideLoginPage();
				GuitarWarehouse.showRegistrationPage();
			}
		});

		guestButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				isAGuest = true;
				hide();
				GuitarWarehouse.showUserMainPage();
			}
		});
	}

	private void checkIfUserExist(String userName, String userPassword) {
		RegistrationPage.greetingService.chekIfUserExist(userName, userPassword, new AsyncCallback<String>() {

			@Override
			public void onSuccess(String result) {
				if (result.equalsIgnoreCase("Welcome")) {
					hide();
					GuitarWarehouse.showUserMainPage();

				} else if (result.equalsIgnoreCase("Invalid credentials")) {
					Window.alert(result);
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.toString());
			}
		});
	}

	public void show() {
		isAGuest = false;
		RootPanel.get().add(loginPanel);
		RootPanel.get().add(loginLabel);
	}

	public void hide() {
		RootPanel.get().remove(loginPanel);
		RootPanel.get().remove(loginLabel);
	}

	public static boolean checkGuestStatus() {
		return isAGuest;
	}
}
