package com.google.gwt.sample.guitarwarehouse.client;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {
	private String name;
	private String password;
	private UserRole role;
	private ArrayList<Guitar> purchases = new ArrayList<Guitar>();

	public User() {
	}

	public User(String userName, String userPassword, UserRole userRole) {
		this.name = userName;
		this.password = userPassword;
		this.role = userRole;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String userPassword) {
		this.password = userPassword;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole userRole) {
		this.role = userRole;
	}

	public ArrayList<Guitar> getPurchases() {
		return purchases;
	}

	public void setPurchases(Guitar guitar) {
		this.purchases.add(guitar);
	}

}
