package com.google.gwt.sample.guitarwarehouse.client;

public enum UserRole {
	ADMIN, USER
}
