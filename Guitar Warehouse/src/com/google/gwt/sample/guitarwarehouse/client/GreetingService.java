package com.google.gwt.sample.guitarwarehouse.client;

import java.io.IOException;
import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {
	String addUserInfoToList(String userName, String userPassword);

	String chekIfUserExist(String userName, String userPassword);

	Guitar addGuitarInfoToFile(Guitar guitarInfo) throws IOException;

	String deleteGuitarInfoFromFile(String guitarName, String guitarColor, String numberOfGuitarStrings,
			String guitarQuantity, String guitarType, String guitarPrice);

	String updateGuitarInfoInsideTheFile(Guitar oldGuitarInfo, Guitar newGuitarInfo);

	String addPurchasesToUser(String userName, Guitar guitar);

	ArrayList<Guitar> getPurchasesOfUser(String userName);
}
