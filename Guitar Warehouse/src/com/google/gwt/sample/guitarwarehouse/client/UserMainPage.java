package com.google.gwt.sample.guitarwarehouse.client;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.TableRowElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class UserMainPage {
	public UserMainPage() {
		buildContent();
	}

	private VerticalPanel userMainPanel = new VerticalPanel();
	private static FlexTable flexTable = new FlexTable();

	private static DialogBox confirmationDialogBox = new DialogBox();
	private VerticalPanel dialogVPanel = new VerticalPanel();
	private static Button yesButton = new Button("Yes");
	private Button noButton = new Button("No");
	private Label confirmationLabel = new Label("Do you want to buy:");
	private static Label guitarInfoConfirmationLabel = new Label();
	private Label quantityConfirmationLabel = new Label("Enter quantity:");
	private static TextBox confirmationQuantityField = new TextBox();
	private HorizontalPanel hPanelForConfirmationButtons = new HorizontalPanel();
	private HorizontalPanel hPanelQuantityConfirmation = new HorizontalPanel();

	private static int tableIndexForBuyButton = 1;

	private static TableRowElement findNearestParentRow(Node node) {
		Node element = findNearestParentNodeByType(node, "tr");
		if (element != null) {
			return element.cast();
		}
		return null;
	}

	private static Node findNearestParentNodeByType(Node node, String nodeType) {
		while ((node != null)) {
			if (Element.is(node)) {
				Element elem = Element.as(node);

				String tagName = elem.getTagName();

				if (nodeType.equalsIgnoreCase(tagName)) {
					return elem.cast();
				}

			}
			node = node.getParentNode();
		}
		return null;
	}

	private void buildContent() {

		HorizontalPanel filterHPanel1 = new HorizontalPanel();
		HorizontalPanel filterHPanel2 = new HorizontalPanel();
		TextBox filterNameField = new TextBox();
		TextBox filterQuantityField = new TextBox();
		ListBox filterColorsListBox = new ListBox();
		RadioButton filterRdButton1 = new RadioButton("filterType", "electric");
		RadioButton filterRdButton2 = new RadioButton("filterType", "acoustic");
		ListBox filterStringsListBox = new ListBox();
		TextBox filterPriceField = new TextBox();
		Button filterPriceButton = new Button("Filter");
		Button filterNameButton = new Button("Filter");
		Button filterColorButton = new Button("Filter");
		Button filterTypeButton = new Button("Filter");
		Button filterQuantityButton = new Button("Filter");
		Button filterStringsButton = new Button("Filter");
		Button showAllTableButton = new Button("Show all");
		Button exitButton = new Button("Exit");
		Button basketButton = new Button("Basket");

		hPanelForConfirmationButtons.add(yesButton);
		hPanelForConfirmationButtons.add(noButton);
		dialogVPanel.add(confirmationLabel);
		dialogVPanel.add(guitarInfoConfirmationLabel);
		hPanelQuantityConfirmation.add(quantityConfirmationLabel);
		hPanelQuantityConfirmation.add(confirmationQuantityField);
		confirmationQuantityField.setWidth("40px");
		confirmationQuantityField.setHeight("10px");
		dialogVPanel.add(hPanelQuantityConfirmation);
		dialogVPanel.add(hPanelForConfirmationButtons);
		dialogVPanel.setSpacing(7);
		confirmationDialogBox.add(dialogVPanel);
		confirmationDialogBox.setText("Confirmation");

		filterHPanel1.add(new Label("Filter by:"));
		filterHPanel1.add(new Label("Name:"));
		filterHPanel1.add(filterNameField);
		filterHPanel1.add(filterNameButton);
		filterHPanel1.add(new Label("Color:"));
		filterHPanel1.add(filterColorsListBox);
		filterHPanel1.add(filterColorButton);
		filterHPanel1.add(new Label("Quantity:"));
		filterHPanel1.add(filterQuantityField);
		filterHPanel1.add(filterQuantityButton);
		filterHPanel1.setSpacing(7);

		filterHPanel2.add(new Label("Type:"));
		filterHPanel2.add(filterRdButton1);
		filterHPanel2.add(filterRdButton2);
		filterHPanel2.add(filterTypeButton);
		filterHPanel2.add(new Label("Strings:"));
		filterHPanel2.add(filterStringsListBox);
		filterHPanel2.add(filterStringsButton);
		filterHPanel2.add(new Label("Price:"));
		filterHPanel2.add(filterPriceField);
		filterHPanel2.add(filterPriceButton);
		filterHPanel2.add(showAllTableButton);
		filterHPanel2.add(basketButton);
		filterHPanel2.add(exitButton);
		filterHPanel2.setSpacing(7);

		filterStringsListBox.addItem("");
		filterStringsListBox.addItem("1");
		filterStringsListBox.addItem("2");
		filterStringsListBox.addItem("3");
		filterStringsListBox.addItem("4");
		filterStringsListBox.addItem("5");
		filterStringsListBox.addItem("6");

		filterColorsListBox.addItem("");
		filterColorsListBox.addItem("red");
		filterColorsListBox.addItem("blue");
		filterColorsListBox.addItem("white");
		filterColorsListBox.addItem("black");

		flexTable.setText(0, 0, "Title");
		flexTable.setText(0, 1, "Color");
		flexTable.setText(0, 2, "Quantity");
		flexTable.setText(0, 3, "Type");
		flexTable.setText(0, 4, "Srings");
		flexTable.setText(0, 5, "Price");
		flexTable.setCellPadding(5);
		flexTable.setBorderWidth(1);
		flexTable.getRowFormatter().addStyleName(0, "watchListHeader");

		userMainPanel.add(filterHPanel1);
		userMainPanel.add(filterHPanel2);
		userMainPanel.add(flexTable);

		exitButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				hide();
				GuitarWarehouse.showLoginPage();
			}
		});

		basketButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (LoginPage.checkGuestStatus()) {
					Window.alert("For buying you have to sign in");
					return;
				}
				hide();
				GuitarWarehouse.showBasketPage();
			}
		});

		filterNameButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (int i = 1; i <= flexTable.getRowCount(); i++) {
					if (!filterNameField.getText().equals("")
							&& !flexTable.getText(i, 0).equals(filterNameField.getText())) {
						flexTable.getRowFormatter().setVisible(i, false);
					}
				}
				filterNameField.setText("");
			}
		});

		filterPriceButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (int i = 1; i <= flexTable.getRowCount(); i++) {
					if (!filterPriceField.getText().equals("")
							&& !flexTable.getText(i, 5).equals(filterPriceField.getText())) {
						flexTable.getRowFormatter().setVisible(i, false);
					}
				}
				filterPriceField.setText("");
			}
		});

		filterColorButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (int i = 1; i <= flexTable.getRowCount(); i++) {
					if (!filterColorsListBox.getSelectedItemText().equals("")
							&& !flexTable.getText(i, 1).equals(filterColorsListBox.getSelectedItemText())) {
						flexTable.getRowFormatter().setVisible(i, false);
					}
				}
			}
		});

		filterQuantityButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (int i = 1; i <= flexTable.getRowCount(); i++) {
					if (!filterQuantityField.getText().equals("")
							&& !flexTable.getText(i, 2).equals(filterQuantityField.getText())) {
						flexTable.getRowFormatter().setVisible(i, false);
					}
				}
				filterQuantityField.setText("");
			}
		});

		filterTypeButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				String filterGuitarType;
				if (filterRdButton1.isChecked()) {
					filterGuitarType = "electric";
				} else if (filterRdButton2.isChecked()) {
					filterGuitarType = "acoustic";
				} else {
					filterGuitarType = "";
				}
				for (int i = 1; i <= flexTable.getRowCount(); i++) {
					if (!flexTable.getText(i, 3).equals(filterGuitarType)) {
						flexTable.getRowFormatter().setVisible(i, false);
					}
				}
				filterRdButton1.setChecked(false);
				filterRdButton2.setChecked(false);
			}
		});

		filterStringsButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (int i = 1; i <= flexTable.getRowCount(); i++) {
					if (!filterStringsListBox.getSelectedItemText().equals("")
							&& !flexTable.getText(i, 4).equals(filterStringsListBox.getSelectedItemText())) {
						flexTable.getRowFormatter().setVisible(i, false);
					}
				}
			}
		});

		showAllTableButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (flexTable.getRowCount() > 1) {
					for (int i = 1; i <= flexTable.getRowCount(); i++) {
						if (!flexTable.getRowFormatter().isVisible(i)) {
							flexTable.getRowFormatter().setVisible(i, true);
						}
					}
				}
			}
		});

		noButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				confirmationQuantityField.setText("");
				confirmationDialogBox.hide();
			}
		});

	}

	public static void setGuitarToUserTable(Guitar guitar) {
		int row = flexTable.getRowCount();
		Button buyButton = new Button("Buy");

		flexTable.setText(row, 0, guitar.getName());
		flexTable.setText(row, 1, guitar.getColor());
		flexTable.setText(row, 2, guitar.getQuantity());
		flexTable.setText(row, 3, guitar.getType());
		flexTable.setText(row, 4, guitar.getNumberOfGuitarStrings());
		flexTable.setText(row, 5, guitar.getPrice());
		flexTable.setWidget(row, 6, buyButton);

		buyButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (LoginPage.checkGuestStatus()) {
					Window.alert("For buying you have to sign in");
					return;
				}
				TableRowElement tableRow = findNearestParentRow(buyButton.getElement());
				tableIndexForBuyButton = tableRow.getRowIndex();
				guitarInfoConfirmationLabel.setText(flexTable.getText(tableIndexForBuyButton, 0) + "  |  color: "
						+ flexTable.getText(tableIndexForBuyButton, 1) + "  |  type: "
						+ flexTable.getText(tableIndexForBuyButton, 3) + "  |  strings: "
						+ flexTable.getText(tableIndexForBuyButton, 4) + "  |  price: "
						+ flexTable.getText(tableIndexForBuyButton, 5) + "?");
				confirmationDialogBox.center();
				confirmationDialogBox.show();
			}
		});

		yesButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				if (!confirmationQuantityField.getText().matches("^[0-9]{1,2}$")) {
					return;
				}
				if (Integer.parseInt(confirmationQuantityField.getText()) > Integer
						.parseInt(flexTable.getText(tableIndexForBuyButton, 2))
						|| confirmationQuantityField.getText().equals("0")) {
					return;
				}
				Integer price = Integer.parseInt(confirmationQuantityField.getText())
						* Integer.parseInt(flexTable.getText(tableIndexForBuyButton, 5));
				int row = BasketPage.getRowCount();
				Integer quantityInBasket = 0;
				for (int i = 1; i < BasketPage.getRowCount(); i++) {
					if (BasketPage.getTableText(i, 0).equals(flexTable.getText(tableIndexForBuyButton, 0))
							&& BasketPage.getTableText(i, 1).equals(flexTable.getText(tableIndexForBuyButton, 1))
							&& BasketPage.getTableText(i, 3).equals(flexTable.getText(tableIndexForBuyButton, 3))
							&& BasketPage.getTableText(i, 4).equals(flexTable.getText(tableIndexForBuyButton, 4))) {
						quantityInBasket = Integer.parseInt(BasketPage.getTableText(i, 2));
						break;
					}
				}

				if (quantityInBasket + Integer.parseInt(confirmationQuantityField.getText()) > Integer
						.parseInt(flexTable.getText(tableIndexForBuyButton, 2))) {
					Window.alert("Incorrect quantity. Please, check quantity of this guitar in your basket");
					return;
				}

				for (int i = 1; i < BasketPage.getRowCount(); i++) {
					if (BasketPage.getTableText(i, 0).equals(flexTable.getText(tableIndexForBuyButton, 0))
							&& BasketPage.getTableText(i, 1).equals(flexTable.getText(tableIndexForBuyButton, 1))
							&& BasketPage.getTableText(i, 3).equals(flexTable.getText(tableIndexForBuyButton, 3))
							&& BasketPage.getTableText(i, 4).equals(flexTable.getText(tableIndexForBuyButton, 4))) {
						Integer newQuantity = Integer.parseInt(BasketPage.getTableText(i, 2))
								+ Integer.parseInt(confirmationQuantityField.getText());
						Integer newPrice = newQuantity * Integer.parseInt(flexTable.getText(tableIndexForBuyButton, 5));
						BasketPage.setTableText(i, 2, newQuantity.toString());
						BasketPage.setTableText(i, 5, newPrice.toString());
						confirmationQuantityField.setText("");
						confirmationDialogBox.hide();
						return;
					}
				}

				BasketPage.setTableText(row, 0, flexTable.getText(tableIndexForBuyButton, 0));
				BasketPage.setTableText(row, 1, flexTable.getText(tableIndexForBuyButton, 1));
				BasketPage.setTableText(row, 2, confirmationQuantityField.getText());
				BasketPage.setTableText(row, 3, flexTable.getText(tableIndexForBuyButton, 3));
				BasketPage.setTableText(row, 4, flexTable.getText(tableIndexForBuyButton, 4));
				BasketPage.setTableText(row, 5, price.toString());
				confirmationQuantityField.setText("");
				confirmationDialogBox.hide();
			}
		});
	}

	public static void deleteGuitarFromUserTable(int row) {
		flexTable.removeRow(row);
	}

	public static void updateGuitarInUserTable(int row, Guitar guitar) {
		flexTable.setText(row, 0, guitar.getName());
		flexTable.setText(row, 1, guitar.getColor());
		flexTable.setText(row, 2, guitar.getQuantity());
		flexTable.setText(row, 3, guitar.getType());
		flexTable.setText(row, 4, guitar.getNumberOfGuitarStrings());
		flexTable.setText(row, 5, guitar.getPrice());
	}

	public static int getRowCount() {
		return flexTable.getRowCount();
	}

	public static String getTableText(int row, int column) {
		return flexTable.getText(row, column);
	}

	public static void setTableText(int row, int column, String text) {
		flexTable.setText(row, column, text);
	}

	public static void removeTableRow(int row) {
		flexTable.removeRow(row);
	}

	public void show() {
		RootPanel.get().add(userMainPanel);
	}

	public void hide() {
		RootPanel.get().remove(userMainPanel);
	}
}
