package com.google.gwt.sample.guitarwarehouse.client;

import java.util.ArrayList;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.TableRowElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class UsersListPage {
	public UsersListPage() {
		buildContent();
	}

	private VerticalPanel usersListPanel = new VerticalPanel();
	private static FlexTable flexTable = new FlexTable();
	private Button goBackButton = new Button("Go back");
	private static DialogBox detailsDialogBox = new DialogBox();
	private VerticalPanel dialogVPanel = new VerticalPanel();
	private static FlexTable detailsFlexTable = new FlexTable();
	private Button okButton = new Button("OK");
	private Label detailsLabel = new Label("Purchases:");

	private static TableRowElement findNearestParentRow(Node node) {
		Node element = findNearestParentNodeByType(node, "tr");
		if (element != null) {
			return element.cast();
		}
		return null;
	}

	private static Node findNearestParentNodeByType(Node node, String nodeType) {
		while ((node != null)) {
			if (Element.is(node)) {
				Element elem = Element.as(node);

				String tagName = elem.getTagName();

				if (nodeType.equalsIgnoreCase(tagName)) {
					return elem.cast();
				}

			}
			node = node.getParentNode();
		}
		return null;
	}

	private void buildContent() {

		flexTable.setText(0, 0, "ID");
		flexTable.setText(0, 1, "Name");
		flexTable.setCellPadding(5);
		flexTable.setBorderWidth(1);
		flexTable.getRowFormatter().addStyleName(0, "watchListHeader");

		usersListPanel.add(goBackButton);
		usersListPanel.add(flexTable);

		dialogVPanel.add(detailsLabel);
		dialogVPanel.add(detailsFlexTable);
		dialogVPanel.add(okButton);

		detailsFlexTable.setText(0, 0, "Title");
		detailsFlexTable.setText(0, 1, "Color");
		detailsFlexTable.setText(0, 2, "Quantity");
		detailsFlexTable.setText(0, 3, "Type");
		detailsFlexTable.setText(0, 4, "Srings");
		detailsFlexTable.setText(0, 5, "Price");
		detailsFlexTable.setCellPadding(5);
		detailsFlexTable.setBorderWidth(1);
		detailsFlexTable.getRowFormatter().addStyleName(0, "watchListHeader");

		detailsDialogBox.setText("Details");
		detailsDialogBox.add(dialogVPanel);

		okButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				detailsDialogBox.hide();
			}
		});

		goBackButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				hide();
				GuitarWarehouse.showAdminMainPage();
			}
		});
	}

	public static void addUserToTable(String name) {
		Integer id = flexTable.getRowCount();
		int row = flexTable.getRowCount();
		flexTable.setText(row, 0, id.toString());
		flexTable.setText(row, 1, name);
		Button detailsButton = new Button("Details");
		detailsButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				TableRowElement tableRow = findNearestParentRow(detailsButton.getElement());
				while (detailsFlexTable.getRowCount() > 1) {
					detailsFlexTable.removeRow(1);
				}
				RegistrationPage.greetingService.getPurchasesOfUser(flexTable.getText(tableRow.getRowIndex(), 1),
						new AsyncCallback<ArrayList<Guitar>>() {

							@Override
							public void onSuccess(ArrayList<Guitar> result) {
								for (Guitar guitar : result) {
									int rowNumberForDetailsTable = detailsFlexTable.getRowCount();
									detailsFlexTable.setText(rowNumberForDetailsTable, 0, guitar.getName());
									detailsFlexTable.setText(rowNumberForDetailsTable, 1, guitar.getColor());
									detailsFlexTable.setText(rowNumberForDetailsTable, 2, guitar.getQuantity());
									detailsFlexTable.setText(rowNumberForDetailsTable, 3, guitar.getType());
									detailsFlexTable.setText(rowNumberForDetailsTable, 4,
											guitar.getNumberOfGuitarStrings());
									detailsFlexTable.setText(rowNumberForDetailsTable, 5, guitar.getPrice());
								}

							}

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub

							}
						});
				detailsDialogBox.show();
				detailsDialogBox.center();
			}
		});
		flexTable.setWidget(row, 2, detailsButton);
	}

	public void show() {
		RootPanel.get().add(usersListPanel);
	}

	public void hide() {
		RootPanel.get().remove(usersListPanel);
	}
}
