package com.google.gwt.sample.guitarwarehouse.client;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.TableRowElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class AdminMainPage {
	public AdminMainPage() {
		buildContent();
	}

	private VerticalPanel adminMainPanel = new VerticalPanel();
	private static FlexTable flexTable = new FlexTable();
	private TextBox nameField = new TextBox();
	private TextBox quantityField = new TextBox();
	private TextBox priceField = new TextBox();
	private RadioButton rdButton1 = new RadioButton("type", "electric");
	private RadioButton rdButton2 = new RadioButton("type", "acoustic");
	private Button addButton = new Button("Add");

	private Button saveButton = new Button("Save");
	private Button canselButton = new Button("Cansel");
	private ListBox stringsListBox = new ListBox();
	private ListBox colorsListBox = new ListBox();

	private int tableIndexForSaveButton = 1;
	private int tableIndexForDeleteButton = 1;

	private Guitar guitar = new Guitar();
	private Guitar oldGuitarInfo = new Guitar();
	private Guitar newGuitarInfo = new Guitar();

	private static TableRowElement findNearestParentRow(Node node) {
		Node element = findNearestParentNodeByType(node, "tr");
		if (element != null) {
			return element.cast();
		}
		return null;
	}

	private static Node findNearestParentNodeByType(Node node, String nodeType) {
		while ((node != null)) {
			if (Element.is(node)) {
				Element elem = Element.as(node);

				String tagName = elem.getTagName();

				if (nodeType.equalsIgnoreCase(tagName)) {
					return elem.cast();
				}

			}
			node = node.getParentNode();
		}
		return null;
	}

	private void buildContent() {

		HorizontalPanel hPanel1 = new HorizontalPanel();
		HorizontalPanel hPanel2 = new HorizontalPanel();
		HorizontalPanel filterHPanel1 = new HorizontalPanel();
		HorizontalPanel filterHPanel2 = new HorizontalPanel();

		TextBox filterNameField = new TextBox();
		TextBox filterQuantityField = new TextBox();
		ListBox filterColorsListBox = new ListBox();
		RadioButton filterRdButton1 = new RadioButton("filterType", "electric");
		RadioButton filterRdButton2 = new RadioButton("filterType", "acoustic");
		ListBox filterStringsListBox = new ListBox();
		TextBox filterPriceField = new TextBox();
		Button filterPriceButton = new Button("Filter");
		Button filterNameButton = new Button("Filter");
		Button filterColorButton = new Button("Filter");
		Button filterTypeButton = new Button("Filter");
		Button filterQuantityButton = new Button("Filter");
		Button filterStringsButton = new Button("Filter");
		Button showAllTableButton = new Button("Show all");
		Button exitButton = new Button("Exit");
		Button usersButton = new Button("Users");

		hPanel1.add(new Label("Name:"));
		hPanel1.add(nameField);
		hPanel1.add(new Label("Type:"));
		hPanel1.add(rdButton1);
		hPanel1.add(rdButton2);
		hPanel1.add(addButton);
		hPanel1.add(usersButton);
		hPanel1.add(saveButton);
		hPanel1.add(canselButton);
		hPanel1.add(exitButton);
		saveButton.setVisible(false);
		canselButton.setVisible(false);
		hPanel1.setSpacing(7);

		hPanel2.add(new Label("Quantity:"));
		hPanel2.add(quantityField);
		hPanel2.add(new Label("Strings:"));
		hPanel2.add(stringsListBox);
		hPanel2.add(new Label("Color:"));
		hPanel2.add(colorsListBox);
		hPanel2.add(new Label("Price:"));
		hPanel2.add(priceField);
		hPanel2.setSpacing(7);

		filterHPanel1.add(new Label("Filter by:"));
		filterHPanel1.add(new Label("Name:"));
		filterHPanel1.add(filterNameField);
		filterHPanel1.add(filterNameButton);
		filterHPanel1.add(new Label("Color:"));
		filterHPanel1.add(filterColorsListBox);
		filterHPanel1.add(filterColorButton);
		filterHPanel1.add(new Label("Quantity:"));
		filterHPanel1.add(filterQuantityField);
		filterHPanel1.add(filterQuantityButton);
		filterHPanel1.setSpacing(7);

		filterHPanel2.add(new Label("Type:"));
		filterHPanel2.add(filterRdButton1);
		filterHPanel2.add(filterRdButton2);
		filterHPanel2.add(filterTypeButton);
		filterHPanel2.add(new Label("Strings:"));
		filterHPanel2.add(filterStringsListBox);
		filterHPanel2.add(filterStringsButton);
		filterHPanel2.add(new Label("Price:"));
		filterHPanel2.add(filterPriceField);
		filterHPanel2.add(filterPriceButton);
		filterHPanel2.add(showAllTableButton);
		filterHPanel2.setSpacing(7);

		stringsListBox.addItem("1");
		stringsListBox.addItem("2");
		stringsListBox.addItem("3");
		stringsListBox.addItem("4");
		stringsListBox.addItem("5");
		stringsListBox.addItem("6");

		filterStringsListBox.addItem("");
		filterStringsListBox.addItem("1");
		filterStringsListBox.addItem("2");
		filterStringsListBox.addItem("3");
		filterStringsListBox.addItem("4");
		filterStringsListBox.addItem("5");
		filterStringsListBox.addItem("6");

		colorsListBox.addItem("red");
		colorsListBox.addItem("blue");
		colorsListBox.addItem("white");
		colorsListBox.addItem("black");

		filterColorsListBox.addItem("");
		filterColorsListBox.addItem("red");
		filterColorsListBox.addItem("blue");
		filterColorsListBox.addItem("white");
		filterColorsListBox.addItem("black");

		rdButton1.setChecked(true);

		flexTable.setText(0, 0, "Title");
		flexTable.setText(0, 1, "Color");
		flexTable.setText(0, 2, "Quantity");
		flexTable.setText(0, 3, "Type");
		flexTable.setText(0, 4, "Srings");
		flexTable.setText(0, 5, "Price");
		flexTable.setCellPadding(5);
		flexTable.setBorderWidth(1);
		flexTable.getRowFormatter().addStyleName(0, "watchListHeader");

		adminMainPanel.add(hPanel1);
		adminMainPanel.add(hPanel2);
		adminMainPanel.add(filterHPanel1);
		adminMainPanel.add(filterHPanel2);
		adminMainPanel.add(flexTable);

		exitButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				hide();
				GuitarWarehouse.showLoginPage();
			}
		});

		addButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (!nameField.getText().matches("^[0-9a-zA-Z\\.]{1,30}$")) {
					Window.alert("'" + nameField.getText() + "' is not a valid symbol.");
					nameField.selectAll();
					return;
				}
				if (!quantityField.getText().matches("^[0-9]{1,2}$") || quantityField.getText().equals("0")
						|| quantityField.getText().equals("00")) {
					Window.alert("Quantity field is numeric field, it should contain a number from 1 to 99");
					quantityField.selectAll();
					return;
				}
				if (!priceField.getText().matches("^[0-9]{1,6}$")) {
					Window.alert("Price field is numeric field");
					priceField.selectAll();
					return;
				}
				guitar.setName(nameField.getText());
				guitar.setColor(colorsListBox.getSelectedItemText());
				guitar.setQuantity(quantityField.getText());
				guitar.setNumberOfGuitarStrings(stringsListBox.getSelectedItemText());
				if (rdButton1.isChecked()) {
					guitar.setType("electric");
				} else {
					guitar.setType("acoustic");
				}
				guitar.setPrice(priceField.getText());
				sendGuitarInfoToServer();
				nameField.setText("");
				quantityField.setText("");
				priceField.setText("");
			}
		});

		filterNameButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (int i = 1; i <= flexTable.getRowCount(); i++) {
					if (!filterNameField.getText().equals("")
							&& !flexTable.getText(i, 0).equals(filterNameField.getText())) {
						flexTable.getRowFormatter().setVisible(i, false);
					}
				}
				filterNameField.setText("");
			}
		});

		filterPriceButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (int i = 1; i <= flexTable.getRowCount(); i++) {
					if (!filterPriceField.getText().equals("")
							&& !flexTable.getText(i, 5).equals(filterPriceField.getText())) {
						flexTable.getRowFormatter().setVisible(i, false);
					}
				}
				filterPriceField.setText("");
			}
		});

		filterColorButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (int i = 1; i <= flexTable.getRowCount(); i++) {
					if (!filterColorsListBox.getSelectedItemText().equals("")
							&& !flexTable.getText(i, 1).equals(filterColorsListBox.getSelectedItemText())) {
						flexTable.getRowFormatter().setVisible(i, false);
					}
				}
			}
		});

		filterQuantityButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (int i = 1; i <= flexTable.getRowCount(); i++) {
					if (!filterQuantityField.getText().equals("")
							&& !flexTable.getText(i, 2).equals(filterQuantityField.getText())) {
						flexTable.getRowFormatter().setVisible(i, false);
					}
				}
				filterQuantityField.setText("");
			}
		});

		filterTypeButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				String filterGuitarType;
				if (filterRdButton1.isChecked()) {
					filterGuitarType = "electric";
				} else if (filterRdButton2.isChecked()) {
					filterGuitarType = "acoustic";
				} else {
					filterGuitarType = "";
				}
				for (int i = 1; i <= flexTable.getRowCount(); i++) {
					if (!flexTable.getText(i, 3).equals(filterGuitarType)) {
						flexTable.getRowFormatter().setVisible(i, false);
					}
				}
				filterRdButton1.setChecked(false);
				filterRdButton2.setChecked(false);
			}
		});

		filterStringsButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (int i = 1; i <= flexTable.getRowCount(); i++) {
					if (!filterStringsListBox.getSelectedItemText().equals("")
							&& !flexTable.getText(i, 4).equals(filterStringsListBox.getSelectedItemText())) {
						flexTable.getRowFormatter().setVisible(i, false);
					}
				}
			}
		});

		showAllTableButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (flexTable.getRowCount() > 1) {
					for (int i = 1; i <= flexTable.getRowCount(); i++) {
						if (!flexTable.getRowFormatter().isVisible(i)) {
							flexTable.getRowFormatter().setVisible(i, true);
						}
					}
				}
			}
		});

		usersButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				hide();
				GuitarWarehouse.showUsersListPage();
			}
		});
	}

	private void sendGuitarInfoToServer() {
		RegistrationPage.greetingService.addGuitarInfoToFile(guitar, new AsyncCallback<Guitar>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.toString());
			}

			@Override
			public void onSuccess(Guitar result) {
				int row = flexTable.getRowCount();
				Button deleteButton = new Button("Delete");
				Button editButton = new Button("Edit");

				flexTable.setText(row, 0, result.getName());
				flexTable.setText(row, 1, result.getColor());
				flexTable.setText(row, 2, result.getQuantity());
				flexTable.setText(row, 3, result.getType());
				flexTable.setText(row, 4, result.getNumberOfGuitarStrings());
				flexTable.setText(row, 5, result.getPrice());
				flexTable.setWidget(row, 6, editButton);
				flexTable.setWidget(row, 7, deleteButton);

				UserMainPage.setGuitarToUserTable(result);

				deleteButton.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						TableRowElement tableRow = findNearestParentRow(deleteButton.getElement());
						tableIndexForDeleteButton = tableRow.getRowIndex();
						flexTable.removeRow(tableIndexForDeleteButton);
						deleteGuitarInfoFromServer(result.getName(), result.getColor(),
								result.getNumberOfGuitarStrings(), result.getQuantity(), result.getType(),
								result.getPrice());
						UserMainPage.deleteGuitarFromUserTable(tableIndexForDeleteButton);
					}
				});

				editButton.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						TableRowElement tableRow = findNearestParentRow(editButton.getElement());
						tableIndexForSaveButton = tableRow.getRowIndex();
						nameField.setText(flexTable.getText(tableRow.getRowIndex(), 0));
						priceField.setText(flexTable.getText(tableRow.getRowIndex(), 5));
						oldGuitarInfo.setPrice(priceField.getText());
						oldGuitarInfo.setName(nameField.getText());
						quantityField.setText(flexTable.getText(tableRow.getRowIndex(), 2));
						oldGuitarInfo.setQuantity(quantityField.getText());
						if (flexTable.getText(tableRow.getRowIndex(), 3).equalsIgnoreCase("electric")) {
							oldGuitarInfo.setType("electric");
							rdButton1.setChecked(true);
						} else {
							oldGuitarInfo.setType("acoustic");
							rdButton2.setChecked(true);
						}
						if (flexTable.getText(tableRow.getRowIndex(), 1).equalsIgnoreCase("red")) {
							colorsListBox.setItemSelected(0, true);
							oldGuitarInfo.setColor("red");
						} else if (flexTable.getText(tableRow.getRowIndex(), 1).equalsIgnoreCase("blue")) {
							colorsListBox.setItemSelected(1, true);
							oldGuitarInfo.setColor("blue");
						} else if (flexTable.getText(tableRow.getRowIndex(), 1).equalsIgnoreCase("white")) {
							colorsListBox.setItemSelected(2, true);
							oldGuitarInfo.setColor("white");
						} else if (flexTable.getText(tableRow.getRowIndex(), 1).equalsIgnoreCase("black")) {
							colorsListBox.setItemSelected(3, true);
							oldGuitarInfo.setColor("black");
						}

						if (flexTable.getText(tableRow.getRowIndex(), 4).equalsIgnoreCase("1")) {
							stringsListBox.setItemSelected(0, true);
							oldGuitarInfo.setNumberOfGuitarStrings("1");
						} else if (flexTable.getText(tableRow.getRowIndex(), 4).equalsIgnoreCase("2")) {
							stringsListBox.setItemSelected(1, true);
							oldGuitarInfo.setNumberOfGuitarStrings("2");
						} else if (flexTable.getText(tableRow.getRowIndex(), 4).equalsIgnoreCase("3")) {
							stringsListBox.setItemSelected(2, true);
							oldGuitarInfo.setNumberOfGuitarStrings("3");
						} else if (flexTable.getText(tableRow.getRowIndex(), 4).equalsIgnoreCase("4")) {
							stringsListBox.setItemSelected(3, true);
							oldGuitarInfo.setNumberOfGuitarStrings("4");
						} else if (flexTable.getText(tableRow.getRowIndex(), 4).equalsIgnoreCase("5")) {
							stringsListBox.setItemSelected(4, true);
							oldGuitarInfo.setNumberOfGuitarStrings("5");
						} else if (flexTable.getText(tableRow.getRowIndex(), 4).equalsIgnoreCase("6")) {
							stringsListBox.setItemSelected(5, true);
							oldGuitarInfo.setNumberOfGuitarStrings("6");
						}

						addButton.setVisible(false);
						saveButton.setVisible(true);
						canselButton.setVisible(true);

						canselButton.addClickHandler(new ClickHandler() {

							@Override
							public void onClick(ClickEvent event) {
								addButton.setVisible(true);
								saveButton.setVisible(false);
								canselButton.setVisible(false);
								nameField.setText("");
								quantityField.setText("");
								priceField.setText("");
							}
						});
						saveButton.addClickHandler(new ClickHandler() {

							@Override
							public void onClick(ClickEvent event) {

								if (!nameField.getText().matches("^[0-9a-zA-Z\\.]{1,30}$")) {
									// Window.alert("'" + nameField.getText() + "' is not a valid symbol.");
									return;
								}
								if (!quantityField.getText().matches("^[0-9]{1,2}$")
										|| quantityField.getText().equals("0")
										|| quantityField.getText().equals("00")) {
									// Window.alert(
									// "Quantity field is numeric field, it should contain a number from 1 to 99");
									return;
								}
								if (!priceField.getText().matches("^[1-9]{1,6}$")) {
									// Window.alert("Price field is numeric field");
									return;
								}
								flexTable.setText(tableIndexForSaveButton, 0, nameField.getText());
								newGuitarInfo.setName(nameField.getText());
								flexTable.setText(tableIndexForSaveButton, 1, colorsListBox.getSelectedItemText());
								newGuitarInfo.setColor(colorsListBox.getSelectedItemText());
								flexTable.setText(tableIndexForSaveButton, 2, quantityField.getText());
								newGuitarInfo.setQuantity(quantityField.getText());
								flexTable.setText(tableIndexForSaveButton, 4, stringsListBox.getSelectedItemText());

								newGuitarInfo.setNumberOfGuitarStrings(stringsListBox.getSelectedItemText());
								if (rdButton1.isChecked()) {
									flexTable.setText(tableIndexForSaveButton, 3, "electric");
									newGuitarInfo.setType("electric");
								} else {
									flexTable.setText(tableIndexForSaveButton, 3, "acoustic");
									newGuitarInfo.setType("acoustic");
								}
								flexTable.setText(tableIndexForSaveButton, 5, priceField.getText());
								newGuitarInfo.setPrice(priceField.getText());
								udateGuitarInfoOnServer(oldGuitarInfo, newGuitarInfo);
								UserMainPage.updateGuitarInUserTable(tableIndexForSaveButton, newGuitarInfo);
								addButton.setVisible(true);
								saveButton.setVisible(false);
								canselButton.setVisible(false);
								nameField.setText("");
								quantityField.setText("");
								priceField.setText("");
							}
						});

					}
				});

			}
		});
	}

	private void deleteGuitarInfoFromServer(String guitarName, String guitarColor, String numberOfGuitarStrings,
			String guitarQuantity, String guitarType, String guitarPrice) {
		RegistrationPage.greetingService.deleteGuitarInfoFromFile(guitarName, guitarColor, numberOfGuitarStrings,
				guitarQuantity, guitarType, guitarPrice, new AsyncCallback<String>() {

					@Override
					public void onSuccess(String result) {
						Window.alert("Guitar info successfully deleted from the server!");
					}

					@Override
					public void onFailure(Throwable caught) {
						Window.alert(caught.toString());
					}
				});
	}

	private void udateGuitarInfoOnServer(Guitar oldGuitarInfo, Guitar newGuitarInfo) {
		RegistrationPage.greetingService.updateGuitarInfoInsideTheFile(oldGuitarInfo, newGuitarInfo,
				new AsyncCallback<String>() {

					@Override
					public void onSuccess(String result) {
						Window.alert("Guitar info successfully updated on the server!");
					}

					@Override
					public void onFailure(Throwable caught) {
						Window.alert(caught.toString());
					}
				});
	}

	public static void setTableText(int row, int column, String text) {
		flexTable.setText(row, column, text);
	}

	public static void removeTableRow(int row) {
		flexTable.removeRow(row);
	}

	public void show() {
		RootPanel.get().add(adminMainPanel);
	}

	public void hide() {
		RootPanel.get().remove(adminMainPanel);
	}
}
